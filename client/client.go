// This file was auto-generated by Fern from our API Definition.

package client

import (
	attempts "github.com/hookdeck/hookdeck-go-sdk/attempts"
	bookmarks "github.com/hookdeck/hookdeck-go-sdk/bookmarks"
	bulkretryevents "github.com/hookdeck/hookdeck-go-sdk/bulkretryevents"
	bulkretryignoredevents "github.com/hookdeck/hookdeck-go-sdk/bulkretryignoredevents"
	bulkretryrequests "github.com/hookdeck/hookdeck-go-sdk/bulkretryrequests"
	connections "github.com/hookdeck/hookdeck-go-sdk/connections"
	core "github.com/hookdeck/hookdeck-go-sdk/core"
	destinations "github.com/hookdeck/hookdeck-go-sdk/destinations"
	events "github.com/hookdeck/hookdeck-go-sdk/events"
	integrations "github.com/hookdeck/hookdeck-go-sdk/integrations"
	issues "github.com/hookdeck/hookdeck-go-sdk/issues"
	issuetriggers "github.com/hookdeck/hookdeck-go-sdk/issuetriggers"
	notifications "github.com/hookdeck/hookdeck-go-sdk/notifications"
	requests "github.com/hookdeck/hookdeck-go-sdk/requests"
	sources "github.com/hookdeck/hookdeck-go-sdk/sources"
	transformations "github.com/hookdeck/hookdeck-go-sdk/transformations"
	http "net/http"
)

type Client struct {
	baseURL    string
	httpClient core.HTTPClient
	header     http.Header

	IssueTriggers          *issuetriggers.Client
	Attempts               *attempts.Client
	Bookmarks              *bookmarks.Client
	Destinations           *destinations.Client
	BulkRetryEvents        *bulkretryevents.Client
	Events                 *events.Client
	BulkRetryIgnoredEvents *bulkretryignoredevents.Client
	Integrations           *integrations.Client
	Issues                 *issues.Client
	Requests               *requests.Client
	BulkRetryRequests      *bulkretryrequests.Client
	Sources                *sources.Client
	Notifications          *notifications.Client
	Transformations        *transformations.Client
	Connections            *connections.Client
}

func NewClient(opts ...core.ClientOption) *Client {
	options := core.NewClientOptions()
	for _, opt := range opts {
		opt(options)
	}
	return &Client{
		baseURL:                options.BaseURL,
		httpClient:             options.HTTPClient,
		header:                 options.ToHeader(),
		IssueTriggers:          issuetriggers.NewClient(opts...),
		Attempts:               attempts.NewClient(opts...),
		Bookmarks:              bookmarks.NewClient(opts...),
		Destinations:           destinations.NewClient(opts...),
		BulkRetryEvents:        bulkretryevents.NewClient(opts...),
		Events:                 events.NewClient(opts...),
		BulkRetryIgnoredEvents: bulkretryignoredevents.NewClient(opts...),
		Integrations:           integrations.NewClient(opts...),
		Issues:                 issues.NewClient(opts...),
		Requests:               requests.NewClient(opts...),
		BulkRetryRequests:      bulkretryrequests.NewClient(opts...),
		Sources:                sources.NewClient(opts...),
		Notifications:          notifications.NewClient(opts...),
		Transformations:        transformations.NewClient(opts...),
		Connections:            connections.NewClient(opts...),
	}
}
